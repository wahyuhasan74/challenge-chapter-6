<div id="top"></div>

<br />

  <h3 align="center">Car Management API</h3>

</div>

### Built With

- ExpressJS
- Sequelize
- Postgres
- Swagger Open API

### Installation

1. Install packages
   ```sh
   yarn install
   ```
2. Next Step

   ```sh
   - setting file database.js
   - sequelize db:create
   - sequelize db:migrate
   - sequelize db:seed:all
   ```

3. Run

   ```sh
   yarn start
   ```

## Info

- Swagger Car Management API

```sh
http://localhost:8000/api/v1/docs/
```

- Super Admin Account

```sh
   email: wahyuhasan74@gmail.com
   password: 1sampai9
```

## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>
